from django.contrib import admin
from receipts.models import ExpenseCategory, Account, Receipt

# Register your models here.


@admin.register(ExpenseCategory)
class ExpenseCategory(admin.ModelAdmin):
    pass


@admin.register(Account)
class TodoListAdmin(admin.ModelAdmin):
    pass


@admin.register(Receipt)
class TodoListAdmin(admin.ModelAdmin):
    pass
